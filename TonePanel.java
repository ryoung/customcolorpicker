package customcolorpicker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;
import javax.swing.JPanel;

public class TonePanel extends JPanel {

    private static final int SELECTOR_SIZE = 7;

    private final int size;

    private Color baseColor;
    private Color selectedColor;
    private Point targetPosition;
    private Consumer<Color> onChangeAction;

    public TonePanel(int size) {
        this.size = size;
        this.targetPosition = new Point(50, 50);
        setPreferredSize(new Dimension(size, size));

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent me) {
                moveTarget(me.getX(), me.getY());
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent me) {
                moveTarget(me.getX(), me.getY());
            }

            @Override
            public void mousePressed(MouseEvent me) {
                moveTarget(me.getX(), me.getY());
            }
        });
    }

    private void moveTarget(int x, int y) {

        if (x > size) {
            x = size;
        }

        if (x < 0) {
            x = 0;
        }

        if (y > size) {
            y = size;
        }

        if (y < 0) {
            y = 0;
        }

        targetPosition = new Point(x, y);
        selectedColor = colorAt(x, y);

        if (onChangeAction != null) {
            onChangeAction.accept(selectedColor);
        }

        repaint();
    }

    public void setBaseColor(Color color) {
        this.baseColor = color;
        this.selectedColor = colorAt(targetPosition.x, targetPosition.y);

        if (onChangeAction != null) {
            onChangeAction.accept(selectedColor);
        }

        repaint();
    }

    @Override
    public void paint(Graphics g) {

        // Paint the gradient
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                g.setColor(colorAt(x, y));
                g.fillRect(x, y, 1, 1);
            }
        }

        int targetX = targetPosition.x - (SELECTOR_SIZE / 2);
        int targetY = targetPosition.y - (SELECTOR_SIZE / 2);

        // Paint the target (selector)
        g.setColor(Color.WHITE);
        g.fillOval(targetX, targetY, SELECTOR_SIZE, SELECTOR_SIZE);
        g.setColor(Color.BLACK);
        g.drawOval(targetX, targetY, SELECTOR_SIZE, SELECTOR_SIZE);
    }

    private Color colorAt(int x, int y) {
        int red = baseColor.getRed();
        int green = baseColor.getGreen();
        int blue = baseColor.getBlue();

        float yScale = ((float) 255 / size);

        float redDiff = 255 - red;
        float greenDiff = 255 - green;
        float blueDiff = 255 - blue;

        float gRed = 255 - (x * (redDiff / size));
        float gGreen = 255 - (x * (greenDiff / size));
        float gBlue = 255 - (x * (blueDiff / size));

        float yDiff = y * yScale;

        int newRed = ColorUtils.limitRGB((int) (gRed - (yDiff)));
        int newGreen = ColorUtils.limitRGB((int) (gGreen - (yDiff)));
        int newBlue = ColorUtils.limitRGB((int) (gBlue - (yDiff)));

        return new Color(newRed, newGreen, newBlue);
    }

    public void setOnChangeAction(Consumer<Color> onChangeAction) {
        this.onChangeAction = onChangeAction;
    }

}

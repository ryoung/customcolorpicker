package customcolorpicker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JSlider;
import javax.swing.plaf.basic.BasicSliderUI;

public class CustomSliderUI extends BasicSliderUI {

    public CustomSliderUI(JSlider b) {
        super(b);
    }

    @Override
    protected Dimension getThumbSize() {
        return new Dimension(15, 10);
    }

    @Override
    public void paintFocus(Graphics grphcs) {
    }
    
    
    @Override
    public void paintTrack(Graphics g) {
        g.setColor(new Color(20,20,20));
        g.fillRect(trackRect.x - 10, trackRect.y + 7, trackRect.width + 20, 8);
    }

    @Override
    public void paintThumb(Graphics g) {
        g.setColor(new Color(150, 150, 150));
        g.fillRect(thumbRect.x + 5, thumbRect.y, thumbRect.width - 10, thumbRect.height);
    }
}

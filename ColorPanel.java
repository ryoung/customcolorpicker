package customcolorpicker;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.function.Consumer;
import javax.swing.JPanel;

public class ColorPanel extends JPanel
{

	private final int height;
	private final Color[] gradient;

	private int selectedY = 0;
	private Color selectedColor = Color.RED;
	private Consumer<Color> onChangeAction;

	public ColorPanel(int height)
	{
		this.height = height;
		this.gradient = new Color[]{
			Color.RED,
			Color.MAGENTA,
			Color.BLUE,
			Color.CYAN,
			Color.GREEN,
			Color.YELLOW,
			Color.RED};

		setPreferredSize(new Dimension(15, height));

		addMouseMotionListener(new MouseMotionAdapter()
		{
			@Override
			public void mouseDragged(MouseEvent me)
			{
				moveSelector(me.getY());
			}
		});

		addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseReleased(MouseEvent me)
			{
				moveSelector(me.getY());
			}

			@Override
			public void mousePressed(MouseEvent me)
			{
				moveSelector(me.getY());
			}
		});
	}

	public static double colorDistance(Color color1,Color color2)
	{
		double redDistance = color1.getRed() - color2.getRed();
		double greenDistance = color1.getGreen() - color2.getGreen();
		double blueDistance = color1.getBlue() - color2.getBlue();
		double distance = Math.sqrt(redDistance * redDistance +
			greenDistance * greenDistance +
			blueDistance * blueDistance);
		return distance;
	}

	private void moveSelector(int y)
	{
		if (y < 0)
		{
			y = 0;
		}

		if (y > height)
		{
			y = height;
		}

		this.selectedColor = colorAt(y);
		this.selectedY = y;

		if (onChangeAction != null)
		{
			onChangeAction.accept(selectedColor);
		}

		repaint();
	}

	public void selectBaseFor(Color c)
	{
		moveSelector(findBaseY(c));
	}

	public int closestYToColor(Color c2)
	{
		double dz = 100000d;
		int y = height;

		for (int i = 0; i < height; i++)
		{
			Color c1 = colorAt(i);

			for (int j = 0; j < height; j++)
			{
				for (int z = 0; z < height; z++)
				{
					Color c3 = colorAt(c1, j, z);
					if (c3.equals(c2))
					{
						return i;
					}
					double dv = colorDistance(c3, c2);
					if (dv < dz)
					{
						dz = dv;
						y = i;
					}
				}
			}
		}
		return y;
	}

	private Color colorAt(Color baseColor, int x, int y) {
		int red = baseColor.getRed();
		int green = baseColor.getGreen();
		int blue = baseColor.getBlue();

		float yScale = ((float) 255 / height);

		float redDiff = 255 - red;
		float greenDiff = 255 - green;
		float blueDiff = 255 - blue;

		float gRed = 255 - (x * (redDiff / height));
		float gGreen = 255 - (x * (greenDiff / height));
		float gBlue = 255 - (x * (blueDiff / height));

		float yDiff = y * yScale;

		int newRed = ColorUtils.limitRGB((int) (gRed - (yDiff)));
		int newGreen = ColorUtils.limitRGB((int) (gGreen - (yDiff)));
		int newBlue = ColorUtils.limitRGB((int) (gBlue - (yDiff)));

		return new Color(newRed, newGreen, newBlue);
	}

	@Override
	public void paint(Graphics g)
	{

		// Paint the gradient
		for (int y = 0; y < height; y++)
		{
			g.setColor(colorAt(y));
			g.fillRect(0, y, 30, 1);
		}

		// Paint the selector
		g.setColor(Color.WHITE);
		g.fillRect(0, selectedY - 1, 50, 2);
		g.setColor(Color.BLACK);
		g.drawRect(0, selectedY - 2, 50, 3);
	}

	private int findBaseY(Color color)
	{
		int colors = gradient.length - 1;
		float sectionHeight = (float) height / gradient.length;
		float falseHeight = sectionHeight * colors;
		float hue = getHue(color.getRed(), color.getGreen(), color.getBlue());

		return (int) (sectionHeight + (falseHeight - (hue * falseHeight) / 360));
	}

	private float getHue(int red, int green, int blue)
	{

		float min = Math.min(Math.min(red, green), blue);
		float max = Math.max(Math.max(red, green), blue);

		if (min == max)
		{
			return 0;
		}

		float hue;

		if (max == red)
		{
			hue = (green - blue) / (max - min);

		}
		else if (max == green)
		{
			hue = 2f + (blue - red) / (max - min);

		}
		else
		{
			hue = 4f + (red - green) / (max - min);
		}

		hue *= 60;

		if (hue < 0)
		{
			hue += 360;
		}

		return hue;
	}

	private Color colorAt(int y)
	{
		int divisionHeight = height / (gradient.length - 1);
		int colorSection = y / divisionHeight;
		int sectionPosition = y % divisionHeight;

		Color current = gradient[colorSection];
		Color next = colorSection < gradient.length - 1 ? gradient[colorSection + 1] : current;

		int redDiff = current.getRed() - next.getRed();
		int greenDiff = current.getGreen() - next.getGreen();
		int blueDiff = current.getBlue() - next.getBlue();

		float redFraction = sectionPosition * (redDiff / divisionHeight);
		float greenFraction = sectionPosition * (greenDiff / divisionHeight);
		float blueFraction = sectionPosition * (blueDiff / divisionHeight);

		int newRed = ColorUtils.limitRGB((int) (current.getRed() - redFraction));
		int newGreen = ColorUtils.limitRGB((int) (current.getGreen() - greenFraction));
		int newBlue = ColorUtils.limitRGB((int) (current.getBlue() - blueFraction));

		return new Color(newRed, newGreen, newBlue);
	}

	public void setOnChangeAction(Consumer<Color> onChangeAction)
	{
		this.onChangeAction = onChangeAction;
	}
}

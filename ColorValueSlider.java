package customcolorpicker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ColorValueSlider extends JPanel {

    private JSlider slider;
    private JTextField input;

    public ColorValueSlider(String labelText) {
        setBackground(new Color(30, 30, 30));
        setLayout(new BorderLayout(10, 0));

        input = new JTextField();
        input.setPreferredSize(new Dimension(40, 30));
        input.setBorder(new EmptyBorder(5, 5, 5, 5));
        input.setBackground(new Color(20, 20, 20));
        input.setForeground(Color.WHITE);
        input.addActionListener(a -> {
            if (input.getText().matches("-?(0|[1-9]\\d*)")) {
                int value = Integer.parseInt(input.getText());
                update(value);
            }
        });

        JPanel leftSide = new JPanel(new BorderLayout());
        leftSide.setBackground(new Color(30, 30, 30));

        JLabel label = new JLabel(labelText);
        label.setForeground(Color.WHITE);

        slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(255);
        slider.setBackground(new Color(30, 30, 30));
        slider.setUI(new CustomSliderUI(slider));
        slider.addChangeListener(c -> {
            update(slider.getValue());
        });

        leftSide.add(label, BorderLayout.NORTH);
        leftSide.add(slider, BorderLayout.SOUTH);

        add(leftSide, BorderLayout.CENTER);
        add(input, BorderLayout.EAST);
    }

    public void update(int value) {
        value = ColorUtils.limitRGB(value);
        slider.setValue(value);
        input.setText(value + "");
    }
}

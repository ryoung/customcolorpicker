package customcolorpicker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class RuneliteColorPicker extends JFrame {

    private final static int FRAME_WIDTH = 400;
    private final static int FRAME_HEIGHT = 400;
    private final static int TONE_PANEL_SIZE = 160;

    private Color baseColor = Color.GREEN;
    private Color previousColor = Color.GREEN;
    private Color selectedColor = Color.GREEN;

    private final TonePanel tonePanel = new TonePanel(TONE_PANEL_SIZE);
    private final ColorPanel colorPanel = new ColorPanel(TONE_PANEL_SIZE);

    private final JPanel beforePreview = new JPanel();
    private final JPanel afterPreview = new JPanel();

    private final ColorValueSlider redSlider = new ColorValueSlider("Red");
    private final ColorValueSlider greenSlider = new ColorValueSlider("Green");
    private final ColorValueSlider blueSlider = new ColorValueSlider("Blue");
    private final ColorValueSlider alphaSlider = new ColorValueSlider("Alpha");

    private final JTextField hexInput = new JTextField();
    private final JTextField rgbaInput = new JTextField();

    public RuneliteColorPicker() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

        JPanel content = new JPanel(new BorderLayout());
        content.setBorder(new EmptyBorder(15, 15, 15, 15));
        content.setBackground(new Color(30, 30, 30));

        JPanel colorSelection = new JPanel(new BorderLayout(15, 0));
        colorSelection.setBackground(new Color(30, 30, 30));

        JPanel leftPanel = new JPanel(new BorderLayout(15, 0));
        leftPanel.setBackground(new Color(30, 30, 30));

        colorPanel.setOnChangeAction(c -> {
            this.baseColor = c;
            tonePanel.setBaseColor(this.baseColor);
        });

        tonePanel.setOnChangeAction(c -> {
            this.selectedColor = c;
            this.afterPreview.setBackground(selectedColor);
            updateSliders();
            updateInputs();
        });

        leftPanel.add(colorPanel, BorderLayout.WEST);
        leftPanel.add(tonePanel, BorderLayout.CENTER);

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.setBackground(new Color(30, 30, 30));

        JPanel contrastPanel = new JPanel(new GridLayout(1, 2));
        contrastPanel.setBackground(new Color(30, 30, 30));

        beforePreview.setBackground(baseColor);
        afterPreview.setBackground(baseColor);

        contrastPanel.add(beforePreview);
        contrastPanel.add(afterPreview);

        JPanel hexContainer = new JPanel(new GridLayout(2, 1, 0, 5));
        hexContainer.setBackground(new Color(30, 30, 30));

        JLabel hexLabel = new JLabel("Hex color:");
        hexLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        hexLabel.setForeground(Color.WHITE);

        hexInput.setBorder(new EmptyBorder(5, 5, 5, 5));
        hexInput.setBackground(new Color(20, 20, 20));
        hexInput.setForeground(Color.WHITE);
        hexInput.addActionListener(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				final String colorStr = e.getActionCommand();
				int  r =  Integer.valueOf( colorStr.substring( 1, 3 ), 16 );
				int  g =  Integer.valueOf( colorStr.substring( 3, 5 ), 16 );
				int  b =  Integer.valueOf( colorStr.substring( 5, 7 ), 16 );

				Color c = new Color(r, g, b);
				System.out.println(colorPanel.closestYToColor(c));
			}
		});

        hexContainer.add(hexLabel);
        hexContainer.add(hexInput);

        JPanel rgbContainer = new JPanel(new GridLayout(2, 1, 0, 5));
        rgbContainer.setBackground(new Color(30, 30, 30));

        JLabel rgbLabel = new JLabel("RGBA color:");
        rgbLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        rgbLabel.setForeground(Color.WHITE);

        rgbaInput.setBorder(new EmptyBorder(5, 5, 5, 5));
        rgbaInput.setBackground(new Color(20, 20, 20));
        rgbaInput.setForeground(Color.WHITE);

        rgbContainer.add(rgbLabel);
        rgbContainer.add(rgbaInput);

        rightPanel.add(contrastPanel);
        rightPanel.add(hexContainer);
        rightPanel.add(rgbContainer);

        JPanel slidersContainer = new JPanel(new GridLayout(4, 1, 0, 10));
        slidersContainer.setBorder(new EmptyBorder(15, 0, 0, 0));
        slidersContainer.setBackground(new Color(30, 30, 30));

        slidersContainer.add(redSlider);
        slidersContainer.add(greenSlider);
        slidersContainer.add(blueSlider);
        slidersContainer.add(alphaSlider);

        colorSelection.add(leftPanel, BorderLayout.WEST);
        colorSelection.add(rightPanel, BorderLayout.CENTER);
        colorSelection.add(slidersContainer, BorderLayout.SOUTH);

        content.add(colorSelection, BorderLayout.NORTH);

        setContentPane(content);

        this.selectedColor = baseColor;
        updateSliders();
        updateInputs();
        tonePanel.setBaseColor(baseColor);
        colorPanel.selectBaseFor(new Color(255, 255, 10));
    }
    
    private void updateSliders() {
        redSlider.update(selectedColor.getRed());
        greenSlider.update(selectedColor.getGreen());
        blueSlider.update(selectedColor.getBlue());
        alphaSlider.update(selectedColor.getAlpha());
    }

    private void updateInputs() {
        hexInput.setText(getSelectedHex());
        rgbaInput.setText(getSelectedRGB());
    }

    private String getSelectedRGB() {
        return selectedColor.getRed() + ", " + selectedColor.getGreen() + ", "
                + selectedColor.getBlue() + ", " + selectedColor.getAlpha();
    }

    private String getSelectedHex() {
        return String.format("#%02x%02x%02x", selectedColor.getRed(),
                selectedColor.getGreen(), selectedColor.getBlue());
    }
}

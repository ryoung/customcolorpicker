package customcolorpicker;

public class ColorUtils {

    private static final int MAX_RGB_VALUE = 255;
    private static final int MIN_RGB_VALUE = 0;

    public static int limitRGB(int value) {
        if (value < MIN_RGB_VALUE) {
            return MIN_RGB_VALUE;
        }

        if (value > MAX_RGB_VALUE) {
            return MAX_RGB_VALUE;
        }

        return value;
    }

}
